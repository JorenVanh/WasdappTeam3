readme wasdapp

1. MySQL Workbench installeren 
2. Jar runnen
3. CI
4. Project laten runnen in Netbeans
------------------------------------------------------------------------
STAP 1. SQL Workbench


1. Installeer MySQL Workbench
2. Klik op de startpagina op de + om een nieuwe connectie te kunnen maken
3. Vul een connection name en wachtwoord in om een nieuwe connectie op te starten.
4. Voer dan het bijgevoegde create script uit om de database aan te maken.
-------------------------------------------------------------------------
STAP 2 : Jar runnen

1. Zorg ervoor dat er java geinstalleerd is op uw machine.
2. De WasDappTeam3 folder bevat 2 mappen. WasDapp folder is de source code van ons project en van geen beland voor dze eindgebruiker.
   De demo folder bevat de runnable jar en de mappen die nodig zijn voor interactie met de applicatie
3. In de demo folder zit ook een config file. Hierin kunnen enkele variabelen aangepast worden
    3.1 de input folder die gemonitord wordt
    3.2 de output folder waarin de resultaten geschreven worden
    3.3 de proccesed folder waarin verwerkte bestanden naar verplaatst worden
    3.4 de url naar de database (momenteel ingesteld op default localhost (schema wasdapp)
    3.5 de login voor de database (root)
    3.6 het password voor de database (root)
4. Run in de demo folder de jar file met java
5. De applicatie is nu aan het kijken voor aanpassingen binnen de input folder en zal de input verwerken naar de output (nadien komen deze files in proccesed).
   Indien er iets is fout gelopen vind je hiervan logs in de error file.
-------------------------------------------------------------------------
STAP 3 : CI

Creër een folder in u c: schijf genaamd gitlab-runner.
Download de binary amd64.
-> Je vindt dit bestand op de de volgende url: https://docs.gitlab.com/runner/install/windows.html
Zet dit bestand in de aangemaakte folder en hernoem dit bestand naar gitlab-runner.exe

Vervolgens druk je op de windows key en zoek je voor cmd, dit voer je uit als administrator(rechtermuisklik en voer uit als administrator).
1. Voer het volgende commando uit:
gitlab-runner.exe register

2. Voer de gitlab url in:
https://gitlab.com 

3. Voer de token in die je gekregen hebt voor deze runner:
Twggu3Eg9yZ3Gz_L5hWN

4. Voer een omschrijving in voor deze runner:
my-runner    <- is een voorbeeld

5. Voer de tags in die geassocieerd zijn met de runner:
my-tag,another-tag      <- is een voorbeeld

6. Voer de runner executor uit:
shell


Vervolgens moeten we de runner installeren en starten. Dit doe je door de volgende commands.

1. Installeren van de runner:
gitlab-runner install

2. Starten van de runner:
gitlab-runner start
------------------------------------------------------------------------
STAP 4 : Project laten runnen 

1. Maak in de map input een nieuw bestand aan(met bestandsextensie .csv of .json) volgens de richtlijnen hieronder.
2. De applicatie zal de input verwerken en een resultaat creeren.
3. In de output word bij het uitvoeren van een operatie een json/pdf gecreerd met dezelfde naam als de input file.



------------------------------------------------------------------------------
errors afhandelen en log

1. Er wordt een csv bestand in de map input gezet.
2. Als er fouten in dit bestand staan dan gaat men deze errors bijhouden.
3. Er wordt een map gemaakt met daarin een tekst bestand die de errors bevat van het document.
3. Deze map wordt in de map errors gezet.
4. Je kan in deze map de fouten zien die er zijn in het toegevoegde csv bestand.

---------------------------------------------------------------------------------------------------------------------
Zoekoperaties uitvoeren via een json bestand
1. json bestand opmaken
    1.1 Je moet als manier toevoegen meegeven
    1.2 Format veld is noodzakelijk om te bepalen hoe je de output wilt
        opties: json/pdf of leeg. indien het leeg is zal output standaard json zijn
    1.3 in de data geef je de verschillende parameters mee waarop je wilt zoeken
    1.4 Als je de parameters leeg laat zal de hele databank worden opgezocht
2. Een voorbeeld van de opbouw van het input json file : {"manier": "zoek","format": "json", "data": {"locatie": "De Koffiehoek", "nummer":"4"}}
3. plaats het bestand in de input folder
4. Zoeken word verwerkt, indien geslaagd bevat de output map je gevraagde resultaat ("naam_inputfile".pdf/.json)
   Indien niet worden de correcte errors gelogd
---------------------------------------------------------------------------------------------------------------------
Toevoegen via json 
1. json bestand opmaken.

    1.1 Je moet bij de manier op toevoegen zetten.
    1.2 Bij format mag je pdf of json schrijven of zelfs leeg laten.
    1.3 Dan kan je een list van objecten meegeven.  
    1.4 Voorbeeld
{
	"manier": "toevoegen",
	"format":"pdf",
	"data": [{
			"id": "25",
			"titel": "De java meester",
			"locatie": "Joren's groep",
			"straat": "Gaston Crommenlaan",
			"nummer": "4",
			"postcode": "9050",
			"gemeente": "Gent",
			"land": "Belgie",
			"omschrijving": "je weet zelf",
			"wiki_link": null,
			"website": null,
			"telefoon": null,
			"email": null,
			"prijs": 0.0,
			"persoon": null
		}
	]

}
2. Zet json bestand in de map input.
3. De objecten in "data" worden doorgestuurd naar de db indien er geen fouten in het bestand zitten.
4. In de output folder komt er een klein json bestand met een boolean of het toevoegen gelukt is.

-----------------------------------------------------------------------------------
Verwijderen via json
1. json bestand opmaken.

    1.1 Je moet bij de manier op verwijderen zetten.
    1.2 Bij de data kan je dan een parameter weergeven met daarin de waarde die je wilt verwijderen. je kan hier meerdere parameters aan toevoegen (zelfde als zoek).
    1.3 Voorbeeld
        {"manier": "verwijder", "data": {"locatie": "De Koffiehoek","titel":"koffiemachine"}}

2. Zet json bestand in de map input.
3. In de output folder komt er een klein json bestand met een boolean of het verwijderen gelukt is.


