/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.wasdapp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.realdolmen.wasdapp.exceptions.Demo;
import com.realdolmen.wasdapp.exceptions.NoQueryPossibleException;
import com.realdolmen.wasdapp.repositories.InformatieRepository;
import com.realdolmen.wasdapp.services.InformatieService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.json.simple.JSONObject;

/**
 *
 * @author JVHBL61
 */
public class JsonToevoegen {
    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(Demo.class.getName());
    private String path;
    InformatieService infoService = new InformatieService(new InformatieRepository());
    Properties prop = new Properties();
    boolean succes = false;
    
    JsonToevoegen(String pathname) {
        this.path = pathname;
        verwerkerToevoegen();
    }
    
    public void verwerkerToevoegen() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            prop.load(new FileInputStream("./settings.cfg"));
            Toevoeger t = objectMapper.readValue(new File(prop.getProperty("input") + "/" + path), Toevoeger.class);
            infoService.insertItems(t.getData());
            succes = true;
        } catch (IOException ex) {
            String msg = "Fout bij verwerking verwijderen";
            LOGGER.error(msg);
        } catch (NoQueryPossibleException ex) {
            LOGGER.error("Fout in de insert querry");
        }
        generateOutput();
        try {
            Files.move(Paths.get(prop.getProperty("input") + "/" + path),Paths.get(prop.getProperty("procces") + "/" + path));
        } catch (IOException ex) {
            LOGGER.error("Fout verplaatsen naar proccesed folder");
        }
    }
    
    private void generateOutput() {
        try {
            FileWriter fileWriter = new FileWriter(prop.getProperty("output") + "/" + path + ".json");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("De toevoeging was een succes: ", succes);
            fileWriter.write(jsonObject.toJSONString());
            fileWriter.flush();
            try {
                fileWriter.close();
            } catch (Exception e) {
                LOGGER.error("Fout bij wegschrijven naar json.");
            }
        } catch (IOException ex) {
            LOGGER.error("Fout bij bereiken van outputmap");
        }
    }
    
}
