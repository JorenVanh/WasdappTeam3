/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.wasdapp;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.realdolmen.wasdapp.exceptions.Demo;
import com.realdolmen.wasdapp.exceptions.NoQueryPossibleException;
import com.realdolmen.wasdapp.repositories.InformatieRepository;
import com.realdolmen.wasdapp.services.InformatieService;
import java.util.HashMap;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.json.simple.JSONObject;

/**
 *
 * @author JVHBL61
 */
public class JsonVerwijder {

    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(Demo.class.getName());
    String path;
    boolean succes;
    InformatieService infoService = new InformatieService(new InformatieRepository());
    Properties prop = new Properties();

    public JsonVerwijder(String pathname) {
        this.path = pathname;
        verwerkerVerwijder();
    }

    private void verwerkerVerwijder() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            prop.load(new FileInputStream("./settings.cfg"));
            JsonNode node = objectMapper.readValue(new File(prop.getProperty("input") + "/" + path), JsonNode.class);
            JsonNode data = node.get("data");
            Iterator<JsonNode> elements = data.elements();
            Iterator<String> fields = data.fieldNames();
            ArrayList<HashMap<String, String>> parameters = new ArrayList<>();
            while (elements.hasNext()) {
                JsonNode dataNode = elements.next();
                HashMap<String, String> map = new HashMap<>();
                map.put(fields.next(), dataNode.asText());
                parameters.add(map);
            }
            succes = infoService.deleteByParameters(parameters);
            generateOutput();
            try {
                Files.move(Paths.get(prop.getProperty("input") + "/" + path), Paths.get(prop.getProperty("procces") + "/" + path));
            } catch (IOException ex) {
                LOGGER.error("Fout verplaatsen naar proccesed folder");
            }
        } catch (IOException ex) {
            LOGGER.error("Fout bij verwerking verwijderen. JSon is waarschijnlijk ongeldig!");
        } catch (NoQueryPossibleException ex) {
            LOGGER.error("Fout bij invoeren delete statement");
        }
    }

    private void generateOutput() {
        try {
            FileWriter fileWriter = new FileWriter(prop.getProperty("output") + "/" + path + ".json");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("De verwijdering was een succes: ", succes);
            fileWriter.write(jsonObject.toJSONString());
            fileWriter.flush();
            try {
                fileWriter.close();
            } catch (Exception e) {
                LOGGER.error("Fout bij wegschrijven naar json.");
            }
        } catch (IOException ex) {
            LOGGER.error("Fout bij bereiken van outputmap");
        }
    }
}
