/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.wasdapp;

import com.realdolmen.wasdapp.domain.Informatie;
import java.util.ArrayList;

/**
 *
 * @author JVHBL61
 */
public class Toevoeger {

    private String manier;
    private String format;
    private ArrayList<Informatie> data;

    public String getManier() {
        return manier;
    }

    public void setManier(String manier) {
        this.manier = manier;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public ArrayList<Informatie> getData() {
        return data;
    }

    public void setData(ArrayList<Informatie> data) {
        this.data = data;
    }

}
