/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.wasdapp;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.WriterException;
import com.realdolmen.wasdapp.domain.Informatie;
import com.realdolmen.wasdapp.exceptions.Demo;
import com.realdolmen.wasdapp.exceptions.NoQueryPossibleException;
import com.realdolmen.wasdapp.repositories.InformatieRepository;
import com.realdolmen.wasdapp.services.InformatieService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author JVHBL61
 */
public class JsonZoek {

    private static final String QR_CODE_IMAGE_PATH = "./";
    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(Demo.class.getName());
    private String path;
    QRGenerator qrgen = new QRGenerator();
    PDDocument doc = new PDDocument();

    InformatieService infoService = new InformatieService(new InformatieRepository());
    private ArrayList<Informatie> result = new ArrayList<>();
    Properties prop = new Properties();

    public JsonZoek(String path) {
        this.path = path;
        verwerkerZoek();
    }

    public void verwerkerZoek() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            prop.load(new FileInputStream("./settings.cfg"));
            JsonNode node = objectMapper.readValue(new File(prop.getProperty("input") + "/" + path), JsonNode.class);
            JsonNode data = node.get("data");
            Iterator<JsonNode> elements = data.elements();
            Iterator<String> fields = data.fieldNames();
            ArrayList<HashMap<String, String>> parameters = new ArrayList<>();
            while (elements.hasNext()) {
                JsonNode dataNode = elements.next();
                HashMap<String, String> map = new HashMap<>();
                map.put(fields.next(), dataNode.asText());
                parameters.add(map);
            }
            result = infoService.findByParameters(parameters);
            generateOutput();
        } catch (IOException ex) {
            String msg = "Fout bij verwerking toevoegen";
            LOGGER.error(msg);
        } catch (NoQueryPossibleException ex) {
            String msg = "Fout in de zoek query, controleer parameters in json!";
            LOGGER.error(msg);
        }
    }

    private void generateOutput() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode node = objectMapper.readValue(new File(prop.getProperty("input") + "/" + path), JsonNode.class);
            JsonNode mode = node.get("format");
            String modeS = mode.asText();
            try {
                Files.move(Paths.get(prop.getProperty("input") + "/" + path), Paths.get(prop.getProperty("procces") + "/" + path));
            } catch (IOException ex) {
                LOGGER.error("Fout verplaatsen naar proccesed folder");
            }
            switch (modeS) {
                case "json":
                    generateJSON();
                    break;
                case "pdf":
                    generatePdf();
                    break;
                default:
                    generateJSON();
                    break;
            }
        } catch (IOException ex) {
            LOGGER.error("Geen format meegegeven in json file");
        }
    }

    public void generatePdf() {
        try {
            if (result != null) {
                PDDocument doc = new PDDocument();
                int x = 0;
                ArrayList<PDPage> paginas = new ArrayList<>();
                for (Informatie i : result) {
                    QRGenerator.generateQRCodeImage(i.getTitel(), 400, 400, QR_CODE_IMAGE_PATH + i.getTitel() + ".png");
                    PDImageXObject qr = PDImageXObject.createFromFile("./" + i.getTitel() + ".png", doc);
                    PDPage page = new PDPage(new PDRectangle(PDRectangle.A4.getHeight(), PDRectangle.A4.getWidth()));
                    doc.addPage(page);
                    paginas.add(page);
                    PDPageContentStream CS = new PDPageContentStream(doc, paginas.get(x), PDPageContentStream.AppendMode.APPEND, true, true);
                    CS.beginText();
                    CS.newLineAtOffset(25, 550);
                    CS.setFont(PDType1Font.TIMES_ROMAN, 13);
                    CS.showText(i.getTitel());
                    CS.newLineAtOffset(650, 0);
                    CS.showText("WASDAPP");
                    CS.newLineAtOffset(-600, -250);
                    CS.showText("-- " + i.getOmschrijving() + " --");
                    CS.newLineAtOffset(-50, -220);
                    CS.showText(i.getLocatie());
                    CS.newLineAtOffset(0, -20);
                    CS.endText();
                    CS.drawImage(qr, 600, 40, 200, 200);
                    CS.close();
                    x++;
                }

                doc.save(prop.getProperty("output") + "/" + path + ".pdf");
                doc.close();
            } else {
                LOGGER.error("Zoek had geen resultaten");
            }

        } catch (WriterException ex) {
            LOGGER.error("kan niet schrijven naar file");
        } catch (IOException ex) {
            LOGGER.error("fout bij ophalen qr img");
        }
    }

    public void generateJSON() {
        try {
            FileWriter fileWriter = new FileWriter(prop.getProperty("output") + "/" + path + ".json");
            JSONArray inform = new JSONArray();
            for (Informatie i : result) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("titel", i.getTitel());
                jsonObject.put("locatie", i.getLocatie());
                jsonObject.put("straat", i.getStraat());
                jsonObject.put("nummer", i.getNummer());
                jsonObject.put("postcode", i.getPostcode());
                jsonObject.put("gemeente", i.getGemeente());
                jsonObject.put("land", i.getLand());
                jsonObject.put("omschrijving", i.getOmschrijving());
                jsonObject.put("wiki", i.getWiki_link());
                jsonObject.put("website", i.getWebsite());
                jsonObject.put("telefoon", i.getTelefoon());
                jsonObject.put("email", i.getEmail());
                jsonObject.put("prijs", i.getPrijs());
                jsonObject.put("persoon", i.getPersoon());
                inform.add(jsonObject);
            }
            fileWriter.write(inform.toJSONString());
            fileWriter.flush();
            try {
                fileWriter.close();
            } catch (Exception e) {
                LOGGER.error("Fout bij wegschrijven naar json.");
            }
        } catch (IOException ex) {
            LOGGER.error("Fout bij bereiken van outputmap");
        }

    }
}
