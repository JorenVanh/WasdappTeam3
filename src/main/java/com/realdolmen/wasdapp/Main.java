/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.wasdapp;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.realdolmen.wasdapp.domain.Informatie;
import com.realdolmen.wasdapp.exceptions.Demo;
import com.realdolmen.wasdapp.exceptions.NoQueryPossibleException;
import com.realdolmen.wasdapp.repositories.InformatieRepository;
import com.realdolmen.wasdapp.services.InformatieService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author JVHBL61
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(Demo.class.getName());

    public static void main(String[] args) {
        try {
            Properties prop = new Properties();
            prop.load(new FileInputStream("./settings.cfg"));
            WatchService watchService = FileSystems.getDefault().newWatchService();
            Path path = Paths.get(prop.getProperty("input"));
            WatchKey watchKey = path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_DELETE,
                    StandardWatchEventKinds.ENTRY_MODIFY);

            while ((watchKey = watchService.take()) != null) {
                for (WatchEvent<?> event : watchKey.pollEvents()) {
                    if (event.kind() == ENTRY_CREATE) {
                        proccesFile(event.context());
                    }
                }
                watchKey.reset();
            }
        } catch (IOException ex) {
            LOGGER.error("fout bij toegang tot folder");
        } catch (InterruptedException ex) {
            LOGGER.error("fout bij monitoren van folder");

        }
    }

    public static void proccesFile(Object context) {
        try {
            Properties prop = new Properties();
            prop.load(new FileInputStream("./settings.cfg"));
            if (context.toString().endsWith(".csv")) {
                InformatieService informatieService = new InformatieService(new InformatieRepository());
                ArrayList<Informatie> info = new ArrayList<>();
                Reader reader = Files.newBufferedReader(Paths.get(prop.getProperty("input") + "/" + context));
                CsvToBean<Informatie> csvToBean = new CsvToBeanBuilder(reader).withType(Informatie.class).withIgnoreLeadingWhiteSpace(true).build();
                Iterator<Informatie> csvUserIterator = csvToBean.iterator();
                while (csvUserIterator.hasNext()) {
                    Informatie infor = csvUserIterator.next();
                    info.add(new Informatie(infor.getId(), infor.getTitel(), infor.getLocatie(), infor.getStraat(), infor.getNummer(), infor.getPostcode(), infor.getGemeente(), infor.getLand(), infor.getOmschrijving(), infor.getWiki_link(), infor.getWebsite(), infor.getTelefoon(), infor.getEmail(), infor.getPrijs(), infor.getPersoon()));
                }
                try {
                    informatieService.insertItems(info);
                } catch (NoQueryPossibleException ex) {
                    LOGGER.error("Fout tijdens invoegen van de data");
                }
                try {
                    Files.move(Paths.get(prop.getProperty("input") + "/" + context), Paths.get(prop.getProperty("procces") + "/" + context));
                } catch (IOException ex) {
                    LOGGER.error("Fout verplaatsen naar proccesed folder");
                }
            } else if (context.toString().endsWith(".json")) {
                String pathname = context.toString();
                ObjectMapper objectMapper = new ObjectMapper();
                JsonNode node = objectMapper.readValue(new File(prop.getProperty("input") + "/" + pathname), JsonNode.class);
                JsonNode mode = node.get("manier");
                String modeS = mode.asText();
                switch (modeS) {
                    case "zoek":
                        new JsonZoek(pathname);
                        break;
                    case "toevoegen":
                        new JsonToevoegen(pathname);
                        break;
                    case "verwijder":
                        new JsonVerwijder(pathname);
                        break;
                    default:
                        LOGGER.error("Er is geen juiste manier meegegeven");
                }
            } else {
                LOGGER.error("Foute extensie van input file");
            }
        } catch (IOException ex) {
            LOGGER.error("Fout bij vinden van config file");
        } catch (IllegalStateException ex) {
            LOGGER.error("Fout bij de state");
        }

    }
}
